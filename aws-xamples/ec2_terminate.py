#!/usr/bin/env python

instance_id="abcd"

import boto3

ec2 = boto3.resource('ec2')
result = ec2.instances.terminate(instance_id)
print result