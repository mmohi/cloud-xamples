import os, io
import sys

from google.cloud import vision, translate
from google.cloud.vision import types

os.environ[
    "GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/mohiuddin/Documents/gitlab/bangla-vision/bangla-vision-project-key.json"

vision_client = vision.ImageAnnotatorClient()
translate_client = translate.Client()

file_name = '/Users/mohiuddin/Documents/gitlab/bangla-vision/image.jpg'

with io.open(file_name, 'rb') as image_file:
    content = image_file.read()

image = types.Image(content=content)

response = vision_client.label_detection(image=image)
labels = response.label_annotations


def translate_text(text, target='en'):
    translate_client = translate.Client()
    result = translate_client.translate(text, target_language=target)
    print(u'{}'.format(result['translatedText']))


for label in labels:
    # print(label.description)
    translate_text(str(label.description).decode('utf-8'), target='bn')