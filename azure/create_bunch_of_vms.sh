dt=`date +"%m-%d-%y"`
rg_name=$1
region="eastus2"
number_of_mgmnt_node=3
number_of_data_node=3
vnet_name="DEMO"
vm_size="Standard_DS3_v2"
admin_username="admin"
admin_password="ChangeMe"
os_image="RedHat:RHEL:6.8:latest"
ipa_os_image="RedHat:RHEL:7.2:latest"

echo $dt

az group create --name $rg_name --location $region

az vm create \
    --resource-group $rg_name \
    --name ansible$dt \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --image $os_image \
    --size Standard_DS2_v2

for i in $(seq 1 $number_of_mgmnt_node)
do 
az vm create \
    --resource-group $rg_name \
    --name MGMNT0$i \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --image $os_image \
    --size $vm_size 
done

for i in $(seq 1 $number_of_data_node)
do 
az vm create \
    --resource-group $rg_name \
    --name DATA0$i \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --image $os_image \
    --size $vm_size 
done

az vm create \
    --resource-group $rg_name \
    --name ipa \
    --admin-username $admin_username \
    --admin-password $admin_password \
    --image $ipa_os_image \
    --size Standard_DS2_v2

az vm list-ip-addresses -g $rg_name --output table